/**
 * @api {get} /positions/{position_id} Show Position
 * @apiVersion 0.1.0
 * @apiName Show Position
 * @apiGroup Positions
 * @apiDescription Show a positions
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "success": true,
*       "dat": {
 *        "id": 24,
 *        "name": "Administrador",
 *        "rate_st": 47.8,
 *        "rate_ot": 91.65,
 *        "max_hrs": 8,
 *        "description": "Administrar",
 *        "created_by": null,
 *        "updated_by": null,
 *        "created_at": "2021-02-18 15:07:09",
 *        "updated_at": "2021-02-18 15:07:36",
 *        "deleted_at": null
*        },
 *      "message": "record_show",
 *      "status": 200
 *    }  
 *
 */
