/**
 * @api {put} /positions/{position_id} Edit Position
 * @apiVersion 0.1.0
 * @apiName Edit Position
 * @apiGroup Positions
 * @apiDescription Edit a position
 *
 * @apiParam {String} name Position name (unique fon user).
*  @apiParam {Number} rate_st Position rate_st.
 * @apiParam {Number} rate_ot Position rate_ot.
 * @apiParam {Number} max_hrs Position max_hrs.
 * @apiParam {Object[]} nameActivity Position array activities.
 * @apiParam {String} activity_name Activity name (unique for a position_id).
 * @apiParam {Number} position_id Activity position_id.
 * 
 * @apiParamExample {json} Request-Example:
 *    {
 *      "name":"Administrador",
 *       "rate_st":47.80,
 *       "rate_ot":91.65,
 *       "max_hrs":8,
 *       "description":"Administrar"
 *    }
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {Object[]} dat  Request data.
 * @apiError {String} message  Request message.
 * @apiError {Number} status  Request status
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "id": 76,
 *            "name": "ten time-tracking"
 *          } 
*         ],
 *       "message": "record_updated,
 *       "status": 200
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200  VALIDATION ERROR ACTIVITY
 *     {
 *       "success": false,
 *       "dat": '',
 *       "message": "record_no_updated",
 *       "status": 200
 *     }
 * 
 */
