/**
 * @api {delete} /aposition/{position_id} Delete Position
 * @apiVersion 0.1.0
 * @apiName Delete Position
 * @apiGroup Positions
 * @apiDescription Delete a positions
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *         "success": true,
 *         "dat": "",
 *         "message": "record_delete",
 *         "status": 200
 *       } 
 *
 */

