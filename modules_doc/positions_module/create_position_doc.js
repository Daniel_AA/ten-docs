/**
 * @api {post} /positions Create Position
 * @apiVersion 0.1.0
 * @apiName Create Position
 * @apiGroup Positions
 * @apiDescription Create a position
 *
 * @apiParam {String} name Position name (unique fon user).
*  @apiParam {Number} rate_st Position rate_st.
 * @apiParam {Number} rate_ot Position rate_ot.
 * @apiParam {Number} max_hrs Position max_hrs.
 * @apiParam {Object[]} nameActivity Position array activities.
 * @apiParam {String} activity_name Activity name (unique for a position_id).
 * @apiParam {Number} position_id Activity position_id.
 * 
 * @apiParamExample {json} Request-Example:
 *    {
 *      "name":"Programador",
 *      "rate_st":47.80,
 *      "rate_ot":91.65,
 *      "max_hrs":8,
 *      "description":"Programar",
 *      "nameActivity":[
*         {
*           "name": "ten time-tracking",
*           "position_id": 1
 *        }
 *      ]
 *    }
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {Object[]} dat  Request data.
 * @apiError {String} message  Request message.
 * @apiError {Number} status  Request status
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": "",
 *       "message": "record_created",
 *       "status": 200
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200  VALIDATION ERROR POSITION
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "name": [
 *              "The name field is required"
 *            ],
 *            "rate_st": [
 *              "The rate_st field is required"
 *            ],
 *            "rate_ot": [
 *              "The rate_ot field is required"
 *            ],
 *            "max_hrs": [
 *              "The max_hrs field is required"
 *            ]
 *          }
 *        ],
 *       "message": "record_save_error",
 *       "status": 200
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200  VALIDATION ERROR ACTIVITY
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "name": [
 *              "The name field is required"
 *            ],
 *            "position_id": [
 *              "The position_id field is required"
 *            ]
 *          }
 *        ],
 *       "message": "record_save_error",
 *       "status": 200
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200  VALIDATION ERROR ACTIVITY
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "name": [
 *              "An activity with this name already exists for this position_id"
 *            ],
 *          }
 *        ],
 *       "message": "record_save_error",
 *       "status": 200
 *     }
 * 
 */
