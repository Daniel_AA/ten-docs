/**
 * @api {get} /positions Get Positions
 * @apiVersion 0.1.0
 * @apiName Get Positions
 * @apiGroup Positions
 * @apiDescription Get all created positions
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": [
 *          {
*             "id": 2,
 *            "name": "Director",
 *            "rate_st": 45.82,
 *            "rate_ot": 91.64,
 *            "max_hrs": 6,
 *            "description": "Nulla tempore temporibus deleniti quibusdam consequuntur fugit.",
 *            "created_by": null,
 *            "updated_by": null,
 *            "created_at": "2021-02-18 11:25:54",
 *            "updated_at": "2021-02-18 11:25:54",
 *            "deleted_at": null,
 *            "activities": [
 *              {
*                 "id": 10,
*                 "name": "Admin Tasks",
 *                "position_id": 2,
 *                "created_by": null,
 *                "updated_by": null,
 *                "created_at": "2021-02-18 11:25:54",
 *                "updated_at": "2021-02-18 11:25:54"
*               },
 *            ]
 *          }
 *        ],
 *       "message": "OK !!!",
 *       "status": 200
 *     }
 */