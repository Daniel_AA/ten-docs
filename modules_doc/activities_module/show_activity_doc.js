/**
 * @api {get} /activities/{activity_id} Show Activity
 * @apiVersion 0.1.0
 * @apiName Show Activity
 * @apiGroup Activities
 * @apiDescription Show an activity
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "id": 1,
 *            "name": "Admin Tasks",
 *            "position_id": 1,
 *            "created_by": 1,
 *            "updated_by": 1,
 *            "created_at": "2021-02-13 15:35:18",
 *            "updated_at": "2021-02-13 15:35:18"
 *          }
 *        ],
 *       "message": "record_show",
 *       "status": 200
 *     }
 *
 */
