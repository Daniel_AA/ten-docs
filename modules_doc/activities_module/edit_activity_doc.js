/**
 * @api {put} /activities/{activity_id} Edit Activity
 * @apiVersion 0.1.0
 * @apiName Edit Activity
 * @apiGroup Activities
 * @apiDescription Edit an activity
 *
 * @apiParam {String} name Activity name (unique for a position_id).
 * @apiParam {Number} position_id Activity position_id.
 * 
 * @apiParamExample {json} Request-Example:
 *    {
 *      "name":"New Activity Name",
 *      "position_id": 2,
 *    }
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {Object[]} dat  Request data.
 * @apiError {String} message  Request message.
 * @apiError {Number} status  Request status
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "id": 2
 *            "name": "New Activity Name",
 *            "position_id": 2,
 *            "created_by": 1,
 *            "updated_by": 1,
 *            "created_at": "2021-02-18 04:06:54",
 *            "updated_at": "2021-02-18 04:55:01"
 *          }
 *        ],
 *       "message": "record_updated",
 *       "status": 200
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400  VALIDATION ERROR
 *     {
 *       "success": false,
 *       "dat": [
 *          {
 *            "name": [
 *              "The name field is required"
 *            ],
 *            "position_id": [
 *              "The position_id field is required"
 *            ]
 *          }
 *        ],
 *       "message": "record_updated_error_validator",
 *       "status": 400
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400  VALIDATION ERROR
 *     {
 *       "success": false,
 *       "dat": [
 *          {
 *            "name": [
 *              "An activity with this name already exists for this position_id"
 *            ],
 *          }
 *        ],
 *       "message": "record_updated_error_validator",
 *       "status": 400
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500  EDIT ACTIVITY ERROR
 *     {
 *       "success": false,
 *       "dat": '',
 *       "message": "record_updated_error",
 *       "status": 500
 *     }
 *
 */
