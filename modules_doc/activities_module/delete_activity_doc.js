/**
 * @api {get} /activities/{activity_id}/destroy Delete Activity
 * @apiVersion 0.1.0
 * @apiName Delete Activity
 * @apiGroup Activities
 * @apiDescription Delete an activity
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": '',
 *       "message": "record_delete",
 *       "status": 200
 *     }
 *
 */
