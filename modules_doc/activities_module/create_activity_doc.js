/**
 * @api {post} /activities Create Activity
 * @apiVersion 0.1.0
 * @apiName Create Activity
 * @apiGroup Activities
 * @apiDescription Create an activity
 *
 * @apiParam {String} name Activity name (unique for a position_id).
 * @apiParam {Number} position_id Activity position_id.
 * 
 * @apiParamExample {json} Request-Example:
 *    {
 *      "name":"Activity Name",
 *      "position_id": 1,
 *    }
 * 
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object[]} dat  Request data.
 * @apiSuccess {String} message  Request message.
 * @apiSuccess {Number} status  Request status.
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {Object[]} dat  Request data.
 * @apiError {String} message  Request message.
 * @apiError {Number} status  Request status
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "dat": [
 *          {
 *            "id": 1
 *            "name": "Activity Name",
 *            "position_id": 1,
 *            "created_by": 1,
 *            "created_at": "2021-02-13 15:35:18",
 *            "updated_at": "2021-02-13 15:35:18"
 *          }
 *        ],
 *       "message": "record_save",
 *       "status": 200
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400  VALIDATION ERROR
 *     {
 *       "success": false,
 *       "dat": [
 *          {
 *            "name": [
 *              "The name field is required"
 *            ],
 *            "position_id": [
 *              "The position_id field is required"
 *            ]
 *          }
 *        ],
 *       "message": "record_save_error_validator",
 *       "status": 400
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400  VALIDATION ERROR
 *     {
 *       "success": false,
 *       "dat": [
 *          {
 *            "name": [
 *              "An activity with this name already exists for this position_id"
 *            ],
 *          }
 *        ],
 *       "message": "record_save_error_validator",
 *       "status": 400
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500  CREATE ACTIVITY ERROR
 *     {
 *       "success": false,
 *       "dat": '',
 *       "message": "record_save_error",
 *       "status": 500
 *     }
 *
 */
