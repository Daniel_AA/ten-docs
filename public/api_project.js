define({
  "name": "ten_docs",
  "version": "0.1.0",
  "description": "Ten documentation",
  "title": "Ten Docs",
  "url": "http://localhost:8000/api/v1",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-02-18T20:26:36.446Z",
    "url": "https://apidocjs.com",
    "version": "0.26.0"
  }
});
