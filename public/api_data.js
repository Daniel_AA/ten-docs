define({ "api": [
  {
    "type": "post",
    "url": "/activities",
    "title": "Create Activity",
    "version": "0.1.0",
    "name": "Create_Activity",
    "group": "Activities",
    "description": "<p>Create an activity</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Activity name (unique for a position_id).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "position_id",
            "description": "<p>Activity position_id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\":\"Activity Name\",\n  \"position_id\": 1,\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"id\": 1\n       \"name\": \"Activity Name\",\n       \"position_id\": 1,\n       \"created_by\": 1,\n       \"created_at\": \"2021-02-13 15:35:18\",\n       \"updated_at\": \"2021-02-13 15:35:18\"\n     }\n   ],\n  \"message\": \"record_save\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400  VALIDATION ERROR\n{\n  \"success\": false,\n  \"dat\": [\n     {\n       \"name\": [\n         \"The name field is required\"\n       ],\n       \"position_id\": [\n         \"The position_id field is required\"\n       ]\n     }\n   ],\n  \"message\": \"record_save_error_validator\",\n  \"status\": 400\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400  VALIDATION ERROR\n{\n  \"success\": false,\n  \"dat\": [\n     {\n       \"name\": [\n         \"An activity with this name already exists for this position_id\"\n       ],\n     }\n   ],\n  \"message\": \"record_save_error_validator\",\n  \"status\": 400\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  CREATE ACTIVITY ERROR\n{\n  \"success\": false,\n  \"dat\": '',\n  \"message\": \"record_save_error\",\n  \"status\": 500\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/activities_module/create_activity_doc.js",
    "groupTitle": "Activities"
  },
  {
    "type": "get",
    "url": "/activities/{activity_id}/destroy",
    "title": "Delete Activity",
    "version": "0.1.0",
    "name": "Delete_Activity",
    "group": "Activities",
    "description": "<p>Delete an activity</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": '',\n  \"message\": \"record_delete\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/activities_module/delete_activity_doc.js",
    "groupTitle": "Activities"
  },
  {
    "type": "put",
    "url": "/activities/{activity_id}",
    "title": "Edit Activity",
    "version": "0.1.0",
    "name": "Edit_Activity",
    "group": "Activities",
    "description": "<p>Edit an activity</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Activity name (unique for a position_id).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "position_id",
            "description": "<p>Activity position_id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\":\"New Activity Name\",\n  \"position_id\": 2,\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"id\": 2\n       \"name\": \"New Activity Name\",\n       \"position_id\": 2,\n       \"created_by\": 1,\n       \"updated_by\": 1,\n       \"created_at\": \"2021-02-18 04:06:54\",\n       \"updated_at\": \"2021-02-18 04:55:01\"\n     }\n   ],\n  \"message\": \"record_updated\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400  VALIDATION ERROR\n{\n  \"success\": false,\n  \"dat\": [\n     {\n       \"name\": [\n         \"The name field is required\"\n       ],\n       \"position_id\": [\n         \"The position_id field is required\"\n       ]\n     }\n   ],\n  \"message\": \"record_updated_error_validator\",\n  \"status\": 400\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400  VALIDATION ERROR\n{\n  \"success\": false,\n  \"dat\": [\n     {\n       \"name\": [\n         \"An activity with this name already exists for this position_id\"\n       ],\n     }\n   ],\n  \"message\": \"record_updated_error_validator\",\n  \"status\": 400\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  EDIT ACTIVITY ERROR\n{\n  \"success\": false,\n  \"dat\": '',\n  \"message\": \"record_updated_error\",\n  \"status\": 500\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/activities_module/edit_activity_doc.js",
    "groupTitle": "Activities"
  },
  {
    "type": "get",
    "url": "/activities",
    "title": "Get Activities",
    "version": "0.1.0",
    "name": "Get_Activities",
    "group": "Activities",
    "description": "<p>Get all created activities</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"id\": 1,\n       \"name\": \"Admin Tasks\",\n       \"position_id\": 1,\n       \"created_by\": 1,\n       \"updated_by\": 1,\n       \"created_at\": \"2021-02-13 15:35:18\",\n       \"updated_at\": \"2021-02-13 15:35:18\"\n     }\n   ],\n  \"message\": \"OK !!!\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/activities_module/get_activities_doc.js",
    "groupTitle": "Activities"
  },
  {
    "type": "get",
    "url": "/activities/{activity_id}",
    "title": "Show Activity",
    "version": "0.1.0",
    "name": "Show_Activity",
    "group": "Activities",
    "description": "<p>Show an activity</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"id\": 1,\n       \"name\": \"Admin Tasks\",\n       \"position_id\": 1,\n       \"created_by\": 1,\n       \"updated_by\": 1,\n       \"created_at\": \"2021-02-13 15:35:18\",\n       \"updated_at\": \"2021-02-13 15:35:18\"\n     }\n   ],\n  \"message\": \"record_show\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/activities_module/show_activity_doc.js",
    "groupTitle": "Activities"
  },
  {
    "type": "post",
    "url": "/positions",
    "title": "Create Position",
    "version": "0.1.0",
    "name": "Create_Position",
    "group": "Positions",
    "description": "<p>Create a position</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Position name (unique fon user).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rate_st",
            "description": "<p>Position rate_st.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rate_ot",
            "description": "<p>Position rate_ot.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "max_hrs",
            "description": "<p>Position max_hrs.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "nameActivity",
            "description": "<p>Position array activities.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "activity_name",
            "description": "<p>Activity name (unique for a position_id).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "position_id",
            "description": "<p>Activity position_id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\":\"Programador\",\n  \"rate_st\":47.80,\n  \"rate_ot\":91.65,\n  \"max_hrs\":8,\n  \"description\":\"Programar\",\n  \"nameActivity\":[\n     {\n       \"name\": \"ten time-tracking\",\n       \"position_id\": 1\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": \"\",\n  \"message\": \"record_created\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200  VALIDATION ERROR POSITION\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"name\": [\n         \"The name field is required\"\n       ],\n       \"rate_st\": [\n         \"The rate_st field is required\"\n       ],\n       \"rate_ot\": [\n         \"The rate_ot field is required\"\n       ],\n       \"max_hrs\": [\n         \"The max_hrs field is required\"\n       ]\n     }\n   ],\n  \"message\": \"record_save_error\",\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200  VALIDATION ERROR ACTIVITY\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"name\": [\n         \"The name field is required\"\n       ],\n       \"position_id\": [\n         \"The position_id field is required\"\n       ]\n     }\n   ],\n  \"message\": \"record_save_error\",\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200  VALIDATION ERROR ACTIVITY\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"name\": [\n         \"An activity with this name already exists for this position_id\"\n       ],\n     }\n   ],\n  \"message\": \"record_save_error\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/positions_module/create_position_doc.js",
    "groupTitle": "Positions"
  },
  {
    "type": "delete",
    "url": "/aposition/{position_id}",
    "title": "Delete Position",
    "version": "0.1.0",
    "name": "Delete_Position",
    "group": "Positions",
    "description": "<p>Delete a positions</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n    \"success\": true,\n    \"dat\": \"\",\n    \"message\": \"record_delete\",\n    \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/positions_module/delete_position_doc.js",
    "groupTitle": "Positions"
  },
  {
    "type": "put",
    "url": "/positions/{position_id}",
    "title": "Edit Position",
    "version": "0.1.0",
    "name": "Edit_Position",
    "group": "Positions",
    "description": "<p>Edit a position</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Position name (unique fon user).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rate_st",
            "description": "<p>Position rate_st.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rate_ot",
            "description": "<p>Position rate_ot.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "max_hrs",
            "description": "<p>Position max_hrs.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "nameActivity",
            "description": "<p>Position array activities.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "activity_name",
            "description": "<p>Activity name (unique for a position_id).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "position_id",
            "description": "<p>Activity position_id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\":\"Administrador\",\n   \"rate_st\":47.80,\n   \"rate_ot\":91.65,\n   \"max_hrs\":8,\n   \"description\":\"Administrar\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": [\n     {\n       \"id\": 76,\n       \"name\": \"ten time-tracking\"\n     } \n    ],\n  \"message\": \"record_updated,\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200  VALIDATION ERROR ACTIVITY\n{\n  \"success\": false,\n  \"dat\": '',\n  \"message\": \"record_no_updated\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/positions_module/edit_position_doc.js",
    "groupTitle": "Positions"
  },
  {
    "type": "get",
    "url": "/positions",
    "title": "Get Positions",
    "version": "0.1.0",
    "name": "Get_Positions",
    "group": "Positions",
    "description": "<p>Get all created positions</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"dat\": [\n     {\n        \"id\": 2,\n       \"name\": \"Director\",\n       \"rate_st\": 45.82,\n       \"rate_ot\": 91.64,\n       \"max_hrs\": 6,\n       \"description\": \"Nulla tempore temporibus deleniti quibusdam consequuntur fugit.\",\n       \"created_by\": null,\n       \"updated_by\": null,\n       \"created_at\": \"2021-02-18 11:25:54\",\n       \"updated_at\": \"2021-02-18 11:25:54\",\n       \"deleted_at\": null,\n       \"activities\": [\n         {\n            \"id\": 10,\n            \"name\": \"Admin Tasks\",\n           \"position_id\": 2,\n           \"created_by\": null,\n           \"updated_by\": null,\n           \"created_at\": \"2021-02-18 11:25:54\",\n           \"updated_at\": \"2021-02-18 11:25:54\"\n          },\n       ]\n     }\n   ],\n  \"message\": \"OK !!!\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/positions_module/get_positions_doc.js",
    "groupTitle": "Positions"
  },
  {
    "type": "get",
    "url": "/positions/{position_id}",
    "title": "Show Position",
    "version": "0.1.0",
    "name": "Show_Position",
    "group": "Positions",
    "description": "<p>Show a positions</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "dat",
            "description": "<p>Request data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Request message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Request status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n   \"success\": true,\n   \"dat\": {\n    \"id\": 24,\n    \"name\": \"Administrador\",\n    \"rate_st\": 47.8,\n    \"rate_ot\": 91.65,\n    \"max_hrs\": 8,\n    \"description\": \"Administrar\",\n    \"created_by\": null,\n    \"updated_by\": null,\n    \"created_at\": \"2021-02-18 15:07:09\",\n    \"updated_at\": \"2021-02-18 15:07:36\",\n    \"deleted_at\": null\n    },\n  \"message\": \"record_show\",\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules_doc/positions_module/show_position_doc.js",
    "groupTitle": "Positions"
  }
] });
